#include "image2D.h"

//==================================
// Constructors
//==================================

Image2D::Image2D() { //Empty
    _width = 0;
    _height = 0;
    _image = 0;
}

Image2D::Image2D(int width, int height) { //Defined size
    _width = width;
    _height = height;
    pixel p;

    int x, y;
    _image = (image) malloc(_height * sizeof(image_row));

    for (y = 0; y < _height; y++) {
        _image[y] = (image_row) malloc(_width * sizeof(pixel));
        for (x = 0; x < _width; x++) {
            _image[y][x] = p;
        }
    }
}

Image2D::Image2D(const Image2D& cpyImg) { //Copy
    _width = cpyImg.getWidth();
    _height = cpyImg.getHeight();

    int x, y;
    _image = (image) malloc(_height * sizeof(image_row));

    for (y = 0; y < _height; y++) {
        _image[y] = (image_row) malloc(_width * sizeof(pixel));
        for (x = 0; x < _width; x++) {
            _image[y][x] = cpyImg.getImage()[y][x];
        }
    }
}

Image2D& Image2D::operator= (const Image2D& cpyImg) { //Attribution
    _width = cpyImg.getWidth();
    _height = cpyImg.getHeight();

    int x, y;
    _image = (image) malloc(_height * sizeof(image_row));

    for (y = 0; y < _height; y++) {
        _image[y] = (image_row) malloc(_width * sizeof(pixel));
        for (x = 0; x < _width; x++) {
            _image[y][x] = cpyImg.getImage()[y][x];
        }
    }
    return *this;
}

Image2D::~Image2D() {
    for (int y = 0; y < _height; y++) {
        free(_image[y]);
    }

    free(_image);
}

//==================================
// Sets & Getters
//==================================

void Image2D::setPixel(int x, int y, pixel p) {
    _image[y][x] = p;
}

void Image2D::setPixel(point pt, pixel p) {
    _image[pt.y][pt.x] = p;
}

pixel Image2D::getPixel(int x, int y) {
    return _image[y][x];
}

image Image2D::getImage() const {
    return _image;
}

int Image2D::getWidth() const {
    return _width;
}

int Image2D::getHeight() const {
    return _height;
}
