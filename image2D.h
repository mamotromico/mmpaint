#ifndef IMAGE2D_H
#define IMAGE2D_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <SDL2/SDL.h>
#include "primitives.h"

class Image2D {
    public:
        Image2D();
        Image2D(int width, int height);
        Image2D(const Image2D& cpyImg);
        Image2D& operator= (const Image2D& cpyImg);
        virtual ~Image2D();

        void setPixel(int x, int y, pixel p);
        void setPixel(point pt, pixel p);

        pixel getPixel(int x, int y);
        image getImage() const;
        int getWidth() const;
        int getHeight() const;
    protected:
        int _width;
        int _height;
        image _image;
    private:
};

#endif // IMAGE2D_H
