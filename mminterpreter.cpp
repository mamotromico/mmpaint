#include "mminterpreter.h"

MMInterpreter::MMInterpreter(SDL_Renderer* sRenderer){
    renderer = sRenderer;
    tileSet = NULL;
    loadTileSet("tile.png");
}

MMInterpreter::~MMInterpreter() {
    if (tileSet != NULL) {
        SDL_DestroyTexture(tileSet);
        tileSet = NULL;
    }
}

void MMInterpreter::loadTileSet(std::string path) {
    SDL_Texture* newTexture = NULL;
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if (loadedSurface == NULL) {
         printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
    } else {
        SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0, 0xFF));
        newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
        if (newTexture == NULL) {
            printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
        }

        SDL_FreeSurface(loadedSurface);
    }
    tileSet = newTexture;
}

void MMInterpreter::renderTile(pixel pix, int x, int y) {
    SDL_Rect tileQuad = {(x)*10, (y)*10, 10, 10};
    SDL_Rect clip = { ((pix.ch%16))*10, ((pix.ch/16))*10, 10, 10 };

    SDL_RenderCopy(renderer, tileSet, &clip, &tileQuad);
}

void MMInterpreter::displayPixel(pixel pix, int x, int y) {
    SDL_SetTextureColorMod(tileSet, pix.c.R, pix.c.G, pix.c.B);

    renderTile(pix, x, y);
}

void MMInterpreter::displayImage(Image2D* img, int sX, int sY) {
    for (int x = 0; x < img->getWidth(); x++) {
        for (int y = 0; y < img->getHeight(); y++) {
            displayPixel(img->getPixel(x, y), x+sX, y+sY);
        }
    }
}
