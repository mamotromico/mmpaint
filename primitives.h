#ifndef PRIMITIVES_H
#define PRIMITIVES_H

struct point {
    int x, y;

    point():x(0),y(0) {

    }

    point(int x, int y):x(x),y(y) {
    }

    point operator+(const point& p) const {
        return point(p.x+x, p.y+y);
    }

    bool operator==(const point& p) const {
        return (x == p.x && y == p.y);
    }
};

struct dimensions {
    int width, height;
};

struct color {
    unsigned char R, G, B;

    color():R(255), G(255), B(255) {

    }

    color(unsigned char nR, unsigned char nG, unsigned char nB):R(nR), G(nG), B(nB) {

    }

    bool operator==(const color& c) const {
        return (R == c.R && G == c.G && B == c.B);
    }

    bool operator!=(const color& c) const {
        return (R != c.R || G != c.G || B != c.B);
    }
};

struct pixel {
    unsigned char ch;
    color c;

    pixel():ch(0), c(255,255,255) {

    }

    pixel(unsigned char nCh, color nC) {
        ch = nCh;
        c = nC;
    }

    pixel operator=(const pixel& px) {;
        return pixel(ch = px.ch, c = px.c);
    }
};

struct rectangle {
    point origin;
    dimensions size;
};

struct vector2d {
    point p1,p2;
};


typedef pixel* image_row;

typedef pixel** image;



#endif // PRIMITIVES_H
