#ifndef MMBUTTON_H
#define MMBUTTON_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "primitives.h"

enum buttonSprite {
    BUTTON_SPRITE_MOUSE_OUT = 0,
    BUTTON_SPRITE_MOUSE_OVER_MOTION = 1,
    BUTTON_SPRITE_MOUSE_DOWN = 2,
    BUTTON_SPRITE_MOUSE_UP = 3,
    BUTTON_SPRITE_TOTAL = 4
};

class MMPanel;

class MMButton
{
public:
    MMButton(MMPanel* bparent, SDL_Texture* bsprite, int x, int y, int btool);
    void handleEvent( SDL_Event* e );
    void render(SDL_Renderer* r);
    const int BUTTON_WIDTH = 30;
    const int BUTTON_HEIGHT = 30;
    buttonSprite currentSprite;
    void deselect();
    int getTool();
private:
    int tool;
    point position;
    SDL_Texture* sprite;
    bool clicked;
    bool justClicked;
    MMPanel* parent;
};

#endif // MMBUTTON_H
