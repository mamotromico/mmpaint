QT += core
QT -= gui

CONFIG += c++11

TARGET = MMPaint
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    draw.cpp \
    image2D.cpp \
    mminterpreter.cpp \
    mmpanel.cpp \
    mmbutton.cpp

HEADERS += \
    primitives.h \
    image2D.h \
    draw.h \
    mminterpreter.h \
    mmpanel.h \
    mmbutton.h
LIBS += \
    -lSDL2 \
    -lSDL2_image
