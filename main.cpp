#include "image2D.h"
#include "draw.h"
#include "mminterpreter.h"
#include "mmpanel.h"
#include <iostream>
#include <SDL2/SDL_image.h>


//Comnetario modificado


const int SCREEN_WIDTH = 1366;
const int SCREEN_HEIGHT = 800;

void imageDrawOrders(MMPanel* panel) {

    Image2D* img = panel->getImage();

    //Set the brush to neutral and fill bg
    color brushColor(0,0,0);
    pixel brush('O',brushColor);
    *img = Draw::fillBG(*img, brush);

    //Draw line
    point p1 = {20, 4};
    point p2 = {30, 49};
    brushColor = {250,200,50};
    brush = {'P', brushColor};
    *img = Draw::drawLine(*img, p1, p2, brush);

    //Draw rect
    point p3 = {0,0};
    dimensions d1 = {29,29};
    brushColor = {255,255,255};
    brush = {'X', brushColor};
    *img = Draw::drawRect(*img,p3,d1,brush);

    //Blip image
    panel->updatePanel();
}

int main(int argc, char **argv) {

    //SDL componentes for screen manipulation
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    //Translate the image format to a pixel image display
    MMInterpreter* interpreter = NULL;

    //Image container with ruler
    MMPanel* panel = NULL;

    //Initialization sequence
    bool quit = false;
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    } else {
        window = SDL_CreateWindow("MMPaint", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (window == NULL) {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        } else {

            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == NULL) {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
            } else {
                interpreter = new MMInterpreter(renderer);
                if (interpreter  == NULL) {
                    printf( "Interpreter could not be created!");
                } else {
                    panel = new MMPanel(renderer, interpreter);
                    if (panel == NULL) {
                        printf( "Panel could not be created!");
                    } else {

                        //Clear and set screen
                        SDL_SetRenderDrawColor(renderer, 0xAA, 0xAA, 0xAA, 0xFF);
                        SDL_RenderClear(renderer);

                        //Create empty image and set on panel
                        Image2D* img = new Image2D(128, 72);
                        panel->setImage(img);

                        //==================================


                        //==================================
                        // Image Drawing orders
                        //==================================

                        imageDrawOrders(panel);

                        //==================================

                        SDL_Event e;
                        //While application is running
                        while( !quit ) {

                            SDL_WaitEvent( &e);

                            if( e.type == SDL_QUIT ) {
                                quit = true;
                            } else if ( e.type == SDL_KEYDOWN ) {
                                if (e.key.keysym.sym == SDLK_ESCAPE) {
                                    quit = true;
                                }
                            } else if( e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP ){
                                panel->handleButtonEvents(&e);
                            }

//                            //Handle events on queue
//                            while( SDL_PollEvent( &e ) != 0 ) {
//                                //User requests quit
//                                if( e.type == SDL_QUIT ) {
//                                    quit = true;
//                                }
//                                else if( e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP ){
//                                    panel->handleButtonEvents(&e);
//                                }

//                            }
                        }
                    }
                }
            }
        }
    }

    free(interpreter);
    free(panel);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
