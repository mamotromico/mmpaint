#include "draw.h"

Draw::Draw() {

}

Image2D Draw::fillBG(Image2D img, pixel bg) {
    Image2D returnImg = img;
    for (int x = 0; x < returnImg.getWidth(); x++ ) {
        for (int y = 0; y < returnImg.getHeight(); y++) {
            returnImg.setPixel(x, y, bg);
        }
    }
    return returnImg;
}

Image2D Draw::drawLine(Image2D img, point start, point finish, pixel p) {
    Image2D returnImg = img;

    int dx = (finish.x - start.x);
    int incx = ((dx>0) - (dx<0));
    dx = std::abs(dx) << 1;

    int dy = (finish.y - start.y);
    int incy = ((dy>0) - (dy<0));
    dy = std::abs(dy) << 1;

    returnImg.setPixel(start, p);
    point pt = {start.x, start.y};

    if (dx >= dy) {
        int err(dy - (dy >> 1));
        while (pt.x != finish.x) {
            if ((err >= 0) && (err || (incx))) {
                err -= dx;
                pt.y += incy;
            }

            err += dy;
            pt.x += incx;

            if (((pt.x < returnImg.getWidth()) && (pt.x>=0)) && ((pt.y < returnImg.getHeight() && (pt.y>=0)))) {
                returnImg.setPixel(pt, p);
            }
        }
    } else {
        int err(dx - (dy >> 1));
        while(pt.y != finish.y ) {
            if ((err >= 0) && (err || (incy))) {
                err -= dy;
                pt.x += incx;
            }

            err += dx;
            pt.y += incy;

            if (((pt.x < returnImg.getWidth()) && (pt.x>=0)) && ((pt.y < returnImg.getHeight() && (pt.y>=0)))) {
                returnImg.setPixel(pt, p);
            }
        }
    }



    return returnImg;
}

Image2D Draw::drawRect(Image2D img, point cornerOrigin, dimensions dim, pixel p) {
    Image2D returnImg = img;
    point corners1 = {cornerOrigin.x, cornerOrigin.y};
    point corners2 = {cornerOrigin.x + dim.width, cornerOrigin.y};
    point corners3 = {cornerOrigin.x + dim.width, cornerOrigin.y + dim.height};
    point corners4 = {cornerOrigin.x, cornerOrigin.y + dim.height};
    returnImg = drawLine(returnImg,corners1,corners2,p);
    returnImg = drawLine(returnImg,corners2,corners3,p);
    returnImg = drawLine(returnImg,corners3,corners4,p);
    returnImg = drawLine(returnImg,corners4,corners1,p);
    return returnImg;
}

Image2D Draw::draw4Poly(Image2D img, point p1, point p2, point p3, point p4, pixel p) {
    Image2D returnImg = img;
    returnImg = drawLine(returnImg,p1,p2,p);
    returnImg = drawLine(returnImg,p2,p3,p);
    returnImg = drawLine(returnImg,p3,p4,p);
    returnImg = drawLine(returnImg,p4,p1,p);
    return returnImg;
}

int dist(int s, int l, int m) {
    return (s*s + l*l - m*m);
}

Image2D Draw::drawCircle(Image2D img, point center, int radius, pixel b) {
    Image2D returnImg = img;

    for (int x=radius, y=0; x>=y; y++) {
        returnImg.setPixel({center.x+x, center.y+y },b);
        returnImg.setPixel({center.x-x, center.y+y },b);
        returnImg.setPixel({center.x+x, center.y-y },b);
        returnImg.setPixel({center.x-x, center.y-y },b);
        returnImg.setPixel({center.x+y, center.y+x },b);
        returnImg.setPixel({center.x-y, center.y+x },b);
        returnImg.setPixel({center.x+y, center.y-x },b);
        returnImg.setPixel({center.x-y, center.y-x },b);

        if (abs(dist(x-1,y+1,radius)) < abs(dist(x,y+1,radius))) {
            x--;
        }

    }

    return returnImg;
}

Image2D Draw::scanFill(Image2D img, point start, pixel p) {
    Image2D returnImg = img;
    pixel old = img.getPixel(start.x,start.y);
    int x1;
    bool checkAbove, checkBelow;
    std::vector<point>stack;

    if (old.c == p.c) {
        return img;
    }


    stack.push_back(start);

    while(!stack.empty()) {
        point newp = stack.back();
        stack.pop_back();
        x1 = newp.x;
        while(x1 >= 0 && returnImg.getPixel(x1,newp.y).c == old.c) {
            x1--;
        }
        x1++;
        checkAbove = checkBelow = false;
        while(x1 < returnImg.getWidth() && returnImg.getPixel(x1,newp.y).c == old.c) {

            returnImg.setPixel(x1,newp.y,p);

            if (!checkAbove && newp.y > 0 && returnImg.getPixel(x1,newp.y-1).c == old.c) {

                point stacking = {x1, newp.y -1};
                stack.push_back(stacking);
                checkAbove = true;

            } else if (checkAbove && newp.y > 0 && returnImg.getPixel(x1,newp.y - 1).c != old.c) {

                checkAbove = false;

            }

            if (!checkBelow && newp.y < returnImg.getHeight() -1 && returnImg.getPixel(x1,newp.y+1).c == old.c) {

                point stacking = {x1, newp.y +1};
                stack.push_back(stacking);
                checkBelow = true;

            } else if (checkBelow && newp.y < returnImg.getHeight() -1 && returnImg.getPixel(x1,newp.y+1).c != old.c) {

                checkBelow = false;
            }

            x1++;

        }
    }

    return returnImg;
}

