#include "mmbutton.h"
#include "mmpanel.h"

MMButton::MMButton(MMPanel* bparent, SDL_Texture* bsprite, int x, int y, int btool)
{
    position.x = x;
    position.y = y;
    sprite = bsprite;
    parent = bparent;
    clicked = false;
    justClicked = false;
    tool = btool;
}

void MMButton::handleEvent(SDL_Event *e) {
     //If mouse event happened
    if( e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP ) {
        //Get mouse position
        int x, y; SDL_GetMouseState( &x, &y );
        //Check if mouse is in button
        bool inside = true;
        if( x < position.x ) {
            inside = false;
        }
        if( x > position.x + BUTTON_WIDTH ) {
            inside = false;
        }
        if( y < position.y ) {
            inside = false;
        }
        if( y > position.y + BUTTON_HEIGHT ) {

            inside = false;
        }
        if( !inside ) {
            currentSprite = BUTTON_SPRITE_MOUSE_OUT;
            if(clicked == false) {
                SDL_SetTextureColorMod(sprite,255,255,255);
            }
        } else {
            //Set mouse over sprite
            switch( e->type ) {
                case SDL_MOUSEBUTTONDOWN:
                    if(e->button.button == SDL_BUTTON_LEFT) {
                        currentSprite = BUTTON_SPRITE_MOUSE_DOWN;
                        if (justClicked == false) {
                            justClicked = true;
                        }
                        if(clicked == false && justClicked == true) {
                            parent->clearButtons();
                            SDL_SetTextureColorMod(sprite,255,255,255);
                            SDL_SetTextureColorMod(sprite,10,200,10);
                            parent->setTool(tool);
                            clicked =  true;                            
                        } else if(justClicked == true && clicked == true) {
                            SDL_SetTextureColorMod(sprite,255,255,255);
                            clicked = false;
                        }
                    }
                break;

                case SDL_MOUSEBUTTONUP:
                    if(e->button.button == SDL_BUTTON_LEFT) {
                        currentSprite = BUTTON_SPRITE_MOUSE_UP;
                        if (justClicked == true) {
                            justClicked = false;
                        }
                    }
                break;

                case SDL_MOUSEMOTION:
                    currentSprite = BUTTON_SPRITE_MOUSE_OVER_MOTION;
                    if(clicked == false) {
                        SDL_SetTextureColorMod(sprite,255,255,255);
                        SDL_SetTextureColorMod(sprite,200,10,10);
                    }
                break;
            }
        }
    }
}

void MMButton::render(SDL_Renderer* r){
   SDL_Rect srect = {0,0,30,30};
   SDL_Rect drect = {position.x, position.y, 30, 30};

   SDL_RenderCopy(r, sprite, &srect, &drect);
}

void MMButton::deselect() {
    SDL_SetTextureColorMod(sprite,255,255,255);
    clicked = false;
}
