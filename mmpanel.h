#ifndef MMWINDOW_H
#define MMWINDOW_H

#include <SDL2/SDL.h>
#include <list>
#include "image2D.h"
#include "mminterpreter.h"

class MMButton;

enum tool {
    NONE = 0,
    PENCIL = 1,
    LINE = 2,
    RECT = 3,
    POLY4 = 4,
    CIRCLE = 5,
    ELPS = 6,
    BUCKET = 7
};

class MMPanel
{
public:
    MMPanel(SDL_Renderer* nRenderer, MMInterpreter* nInterpreter);
    void setImage(Image2D* nImg);
    Image2D* getImage();
    void handleButtonEvents(SDL_Event* e);
    void updatePanel();
    void clearButtons();
    void setTool(int newTool);
private:
    int offset = 4;
    void drawGrid();
    void loadButtons();
    void drawButtons();

    bool clicked;
    std::list<point>* pointList;

    color brushColor;
    pixel brush;
    tool currentTool;   

    SDL_Renderer* renderer;
    Image2D* image;
    MMInterpreter* painter;
    MMButton* pencilButton;
    MMButton* lineButton;
    MMButton* rectButton;
    MMButton* circleButton;
    MMButton* bucketButton;
    MMButton* poly4Button;
};

#endif // MMWINDOW_H
