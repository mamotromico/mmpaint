#ifndef MMPAINTINTERPRETER_H
#define MMPAINTINTERPRETER_H

#include <SDL2/SDL.h>
#include "image2D.h"
#include <SDL2/SDL_image.h>

class MMInterpreter
{
public:
    MMInterpreter(SDL_Renderer *sRenderer);
    ~MMInterpreter();
    void displayImage(Image2D* img, int sX, int sY);
    void displayPixel(pixel pix, int x, int y);
private:
    SDL_Texture* tileSet;
    SDL_Renderer* renderer;
    void loadTileSet(std::string path);
    void renderTile(pixel pix, int x, int y);
};

#endif // MMPAINTINTERPRETER_H
