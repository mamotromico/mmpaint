#ifndef DRAW_H
#define DRAW_H

#include "image2D.h"

class Draw {
public:
    static Image2D fillBG(Image2D img, pixel bg);
    static Image2D drawLine(Image2D img, point start, point finish, pixel p);
    static Image2D drawRect(Image2D img, point cornerOrigin, dimensions dim, pixel p);
    static Image2D draw4Poly(Image2D img, point p1, point p2, point p3, point p4, pixel p);
    static Image2D drawCircle(Image2D img, point p1, int radius, pixel p);
    static Image2D scanFill(Image2D img, point start, pixel p);

private:
    Draw();

};

#endif // DRAW_H
