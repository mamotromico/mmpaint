#include "mmpanel.h"
#include "mmbutton.h"
#include "draw.h"

MMPanel::MMPanel(SDL_Renderer* nRenderer, MMInterpreter *nInterpreter) {
    renderer = nRenderer;
    painter = nInterpreter;
    image = NULL;
    currentTool = NONE;
    loadButtons();
}

void MMPanel::drawGrid() {
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_Rect stroke = {9+40,9+40,image->getWidth()*10+2,image->getHeight()*10+2};
    SDL_RenderDrawRect(renderer, &stroke);

    brushColor = {0,0,0};
    brush = {'1', brushColor};
    for (int i = 0; i < image->getWidth(); i++) {
        painter->displayPixel(brush,(i+1)+offset,0+offset);
        brush.ch++;
        if (brush.ch > '9') {
            brush.ch = '0';
        }
    }
    brush = {'1', brushColor};
    for (int j = 0; j < image->getHeight(); j++) {
        painter->displayPixel(brush,0+offset,(j+1)+offset);
        brush.ch++;
        if (brush.ch > '9') {
            brush.ch = '0';
        }
    }

    //return to default brush
    brushColor = {255,255,255};
    brush = {'X', brushColor};
}

void MMPanel::loadButtons() {
        SDL_Texture* newTexture = NULL;
        std::string path = "pencil.png";
        SDL_Surface* loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        pencilButton = new MMButton(this, newTexture,41,1, PENCIL);

        newTexture = NULL;
        path = "line.png";
        loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        lineButton = new MMButton(this, newTexture,81,1, LINE);

        newTexture = NULL;
        path = "rect.png";
        loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        rectButton = new MMButton(this, newTexture,121,1, RECT);

        newTexture = NULL;
        path = "circle.png";
        loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        circleButton = new MMButton(this, newTexture,161,1, CIRCLE);

        newTexture = NULL;
        path = "poly4.png";
        loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        poly4Button = new MMButton(this, newTexture,241,1, POLY4);

        newTexture = NULL;
        path = "bucket.png";
        loadedSurface = IMG_Load(path.c_str());
        if (loadedSurface == NULL) {
             printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
        } else {
            newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
            if (newTexture == NULL) {
                printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
            }

            SDL_FreeSurface(loadedSurface);
        }

        bucketButton = new MMButton(this, newTexture,201,1, BUCKET);
}

void MMPanel::drawButtons(){
    pencilButton->render(renderer);
    lineButton->render(renderer);
    rectButton->render(renderer);
    circleButton->render(renderer);
    bucketButton->render(renderer);
}

void MMPanel::setImage(Image2D* nImage) {
    image = nImage;
    drawGrid();
    drawButtons();
}

Image2D* MMPanel::getImage(){
    return image;
}

void MMPanel::handleButtonEvents(SDL_Event* e) {
    if((e->button.x > 49 && e->button.x <= image->getWidth()*10+49) && (e->button.y > 49 && e->button.y <= image->getHeight()*10+49)) {
        point pix = {(e->button.x/10)-5, (e->button.y/10)-5};
        switch(currentTool) {
        case NONE:
            break;
        case PENCIL:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        image->setPixel(pix, brush);
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case LINE:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 2 ) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            point pix2 = pointList->front();
                            pointList->pop_front();
                            *image = Draw::drawLine(*image, pix1, pix2 ,brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case RECT:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 2 ) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            point pix2 = pointList->front();
                            pointList->pop_front();
                            dimensions dim = { pix2.x - pix1.x, pix2.y - pix1.y };
                            *image = Draw::drawRect(*image, pix1, dim,brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case POLY4:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 4 ) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            point pix2 = pointList->front();
                            pointList->pop_front();
                            point pix3 = pointList->front();
                            pointList->pop_front();
                            point pix4 = pointList->front();
                            pointList->pop_front();
                            *image = Draw::draw4Poly(*image, pix1, pix2, pix3, pix4 ,brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case CIRCLE:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 2 ) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            point pix2 = pointList->front();
                            pointList->pop_front();
                            int radius = std::min(abs((pix1.x - pix2.x)/2),abs((pix1.y - pix2.y)/2));
                            point center = {std::min(pix1.x,pix2.x)+radius, std::min(pix1.y,pix2.y)+radius};
                            *image = Draw::drawCircle(*image, center, radius ,brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case ELPS:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 4 ) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            point pix2 = pointList->front();
                            pointList->pop_front();
                            point pix3 = pointList->front();
                            pointList->pop_front();
                            point pix4 = pointList->front();
                            pointList->pop_front();
                            *image = Draw::draw4Poly(*image, pix1, pix2, pix3, pix4 ,brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        case BUCKET:
            if(e->type == SDL_MOUSEBUTTONDOWN) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == false) {
                        pointList->push_back(pix);
                        if(pointList->size() == 1) {
                            point pix1 = pointList->front();
                            pointList->pop_front();
                            *image = Draw::scanFill(*image, pix1, brush);
                        }
                        clicked = true;
                    }
                }
            } else if (e->type == SDL_MOUSEBUTTONUP) {
                if(e->button.button == SDL_BUTTON_LEFT) {
                    if (clicked == true) {
                        clicked = false;
                    }
                }
            }
            break;
        default:
            break;
        }
    } else {
        pencilButton->handleEvent(e);
        lineButton->handleEvent(e);
        rectButton->handleEvent(e);
        circleButton->handleEvent(e);
        bucketButton->handleEvent(e);
        poly4Button->handleEvent(e);
    }
    updatePanel();
}

void MMPanel::updatePanel() {
    SDL_SetRenderDrawColor(renderer, 0xAA, 0xAA, 0xAA, 0xFF);
    SDL_RenderClear(renderer);
    drawGrid();
    drawButtons();
    painter->displayImage(image,1+offset,1+offset);   
    SDL_RenderPresent(renderer);
}

void MMPanel::clearButtons() {
    pencilButton->deselect();
    lineButton->deselect();
    rectButton->deselect();
    circleButton->deselect();
    bucketButton->deselect();
    poly4Button->deselect();
    setTool(NONE);
    updatePanel();
}

void MMPanel::setTool(int newTool) {
    currentTool = (tool)newTool;
    pointList = new std::list<point>();
}
